# AWSPlayGround

## Install AWS, SAM CLI and get credentials set up.

```
sam build
sam deploy --guided
    Stack Name: dev
    AWS Region: us-east-1 //This region supports SMS with SNS, which is in a limited number of regions
    Parameter Email: Email you want to subscribe SNS topic to
    Parameter Phone: Full phone with country code (+16141234567)
    Capabilities: CAPABILITY_IAM
    Save arguments to samconfig.toml (Yes will mean you can run sam deploy and it'll have your info stored for next time)
```

To test:
Go to APIG